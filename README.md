![picture](https://i.imgur.com/O2Ps5Qh.png)

Unofficial Mewe application, Open source and multi-platform for all platforms to use.

Chat and socialise and interact with all your friends on the Mewe desktop app 

&nbsp;&nbsp;&nbsp;&nbsp;

![picture](https://i.imgur.com/MKLz9QK.png)

![picture](https://i.imgur.com/wCeaS6I.png)

 &nbsp;&nbsp;&nbsp;&nbsp;

  You can install Mewe from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/mewe/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/mewedesktop/application/-/releases)

 ### Author
  * Corey Bruce
